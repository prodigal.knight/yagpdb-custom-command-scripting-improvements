# YAGPDB Custom Command Scripting Improvements

## Requirements

You will need to have a userscript managing extension (e.g.: Tampermonkey) and a userstyle managing extension (e.g.: Stylus) installed in your browser in order to use these files.

## Installation

To install, select the `main` branch and view the raw files `ide.user.js` and `style.user.css`. Your userscript/userstyle extensions should automatically detect the files as installable.

## Usage

There are several variables provided in this userscript which may be modified in the GUI at runtime, and are saved across sessions. Most are self-explanatory but a description of the variables and their intended purpose, etc. is provided here for reference.

When making changes to these variables in the GUI, their effect should be applied immediately.

| Variable                       | Type       | Description |
|-------------------------------:|:----------:|:------------|
| `RULERS`                       | `number[]` | The column(s) at which to draw code rulers, so you have an idea of how long your lines are getting. |
| `trimTrailingWhitespaceOnSave` | `boolean`  | Whether or not to trim trailing whitespace from the end of each line when saving via `Ctrl`+`S` |
| `ensureNewlineAtEofOnSave`     | `boolean`  | Whether or not to enforce having a blank line at the end of the text area when saving via `Ctrl`+`S` |
| `theme`                        | `string`   | The color theme to apply to the editing area. Currently supported themes are `monokai` and `nord`. More may be added in future updates. |
| `indentStyle`                  | `string`   | The character to use for indentation. Possible values are `' '` and `'\t'`.
| `indentSize`                   | `number`   | The indentation size. Spaces are repated `indentSize` times, and tabs are displayed as being `indentSize` spaces wide.

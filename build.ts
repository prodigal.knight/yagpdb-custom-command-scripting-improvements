import { transform } from 'https://deno.land/x/swc@0.2.1/mod.ts';

// Run using `deno run --allow-read --allow-net --allow-write build.ts`
try {
  const production = Deno.args.includes('--production') || Deno.args.includes('-p');

  const ts = await Deno.readFile('./ide.user.ts');
  const source = new TextDecoder().decode(ts);

  const userscriptMetaCommentStart = '// ==UserScript==';
  const userscriptMetaCommentEnd = '// ==/UserScript=='
  const userscriptMetaCommentStartIdx = source.indexOf(userscriptMetaCommentStart);
  const userscriptMetaCommentEndIdx = source.indexOf(userscriptMetaCommentEnd) + userscriptMetaCommentEnd.length;
  const userscriptMetaComment = production
    ? source.slice(userscriptMetaCommentStartIdx, userscriptMetaCommentEndIdx + 1)
    : '';

  const result = transform(source, {
    jsc: {
      target: 'es2022',
      parser: {
        syntax: 'typescript',
      },
      minify: production
        ? {
          compress: true,
          mangle: {
            props: {
              reserved: [],
              undeclared: true,
              regex: ".*"
            },
            // @ts-ignore: Not sure why Deno thinks this doesn't exist in the config, the script doesn't complain at runtime
            topLevel: true,
            keepClassNames: false,
            keepFnNames: false,
            keepPrivateProps: false,
            reserved: ['setValue', 'getValue'],
          },
        }
        : undefined,
    },
  });

  const output = userscriptMetaComment + result.code;

  await Deno.writeFile('./ide.user.js', new TextEncoder().encode(output));

  Deno.exit(0);
} catch (e) {
  console.error(e);
  Deno.exit(1);
}

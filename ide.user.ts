// NOTE: Any comments before `declare` statements get stripped from `swc` output
/** Let Deno/TS know about the GM constant available to userscripts and the functions on it that this script attempts to use */
declare const GM: {
  getValue<T>(name: string, defaultValue?: T): Promise<T>;
  setValue(name: string, value: unknown): Promise<void>;
};

// ==UserScript==
// @name        YAGPDB Custom Commands Syntax Highlighting
// @description Adds syntax highlighting to YAGPDB Custom Command editor (as well as a few other handy features)
// @version     1.4.4
// @grant       GM.getValue
// @grant       GM.setValue
// @match       https://yagpdb.xyz/manage/*/customcommands/commands/*
// ==/UserScript==

(async () => {
  const { getValue, setValue } = GM;

  // function isAlpha(char: string) {
  //   return 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.includes(char);
  // }
  // function isNumeric(char: string) {
  //   return '0123456789'.includes(char);
  // }
  // function isAlnum(char: string) {
  //   return isAlpha(char) || isNumeric(char);
  // }
  // function isPunct(char: string) {
  //   return '.,!@#$%^&*(){}?+<>[]/=\\;'.includes(char);
  // }
  // function isWhitespace(char: string) {
  //   return ' \t\n'.includes(char);
  // }

  function assertExists<T>(v: T | void): T {
    if (v === null || v === undefined) {
      throw new Error(`Expected a value, got "${v}"`)
    }

    return v;
  }

  function *elementDescriptorTokenizer(descriptor: string) {
    const l = descriptor.length;

    let start = 0;
    let end = 0;

    out:
    while (end < l) {
      const c = descriptor.charAt(end);

      switch (c) {
        case '.': /* falls through */
        case '#': /* falls through */
        case ' ':
          yield descriptor.slice(start, end);
          yield c;
          start = ++end;
          if (c === ' ') {
            break out;
          }
          /* falls through */
        default:
          end++;
      }
    }

    yield descriptor.slice(start);
  }

  /**
   * Parse an element descriptor string in the form tagName[#id][...classes],
   * e.g. span#id.class1.class2 and return an HTML element corresponding to the
   * descriptor
   *
   * @param  {string} rawDescriptor A descriptor for an HTML tag with optional tag name, id, and classes
   *
   * @return {T}
   */
  function element<T extends HTMLElement = HTMLDivElement>(
    rawDescriptor: string | TemplateStringsArray = '',
    attributes: Record<string, string | number | boolean> = {},
  ) {
    const descriptor = (rawDescriptor as TemplateStringsArray).raw?.join('') ?? rawDescriptor;
    const tokenizer = elementDescriptorTokenizer(descriptor);
    let it: IteratorResult<string>;

    let tag = '';
    let id = '';
    const classes: string[] = [];
    let textContent = '';

    while (!(it = tokenizer.next()).done) {
      if (it.value === '#') {
        id = assertExists((it = tokenizer.next()).value);
      } else if (it.value === '.') {
        classes.push(assertExists((it = tokenizer.next()).value));
      } else if (it.value === ' ') {
        textContent = assertExists((it = tokenizer.next()).value);
      } else {
        tag = assertExists(it.value);
      }
    }

    const el = document.createElement(tag || 'div') as T;
    if (id) {
      el.setAttribute('id', id);
    }
    if (classes.length) {
      el.classList.add(...classes);
    }

    for (const [name, value] of Object.entries(attributes)) {
      if (value !== false) {
        el.setAttribute(name, value.toString());
      }
    }

    if (textContent) {
      el.appendChild(document.createTextNode(textContent));
    }

    return el;
  }

  /** No-op function for use as a stand-in function call with no side effects */
  const noop = () => {};

  /** Unique symbol to prevent direct instantiation of Value objects */
  // NOTE: It's not really effective when everything is in a single script like
  // this but I prefer to do it as standard practice "just in case"
  const ValueLock = Symbol('ValueLock');

  /**
   * Wrapper for GM.getValue and GM.setValue calls which caches the value
   */
  class Value<T = unknown> {
    /**
     * Creates a generic Value object which can hold any data
     *
     * @param  {string}               name         The name of the value which is held in storage from session to session
     * @param  {K}                    defaultValue The default value to use if none is set
     * @param  {((v: K) => boolean)?} validator    A validation function which is used to ensure new values are valid before calling GM.setValue
     *
     * @return {Value<K>}
     */
    public static for<K>(name: string, defaultValue: K, validator?: ((v: K) => boolean)): Promise<Value<K>> {
      return this.forImpl(new Value(ValueLock, name, defaultValue), validator);
    }

    /**
     * Creates a generic Value object which can hold data that matches the specified values
     *
     * @param  {string} name         The name of the value which is held in storage from session to session
     * @param  {K}      defaultValue The default value to use if none is set
     * @param  {K[]}    values       The valid values which are allowed to exist in this Value
     *
     * @return {Value<K>}
     */
    public static withAllowedValues<K>(name: string, defaultValue: K, values: K[]) {
      return this.forImpl(new Value(ValueLock, name, defaultValue), (v: K) => values.includes(v));
    }

    /**
     * Creates a generic Value object which holds a boolean value
     *
     * @param  {string}            name         The name of the value which is held in storage from session to session
     * @param  {boolean?}          defaultValue The default value to use if none is set. Default: true
     *
     * @return {Value<boolean>}
     */
    public static boolean(name: string, defaultValue = true) {
      return this.withAllowedValues(name, defaultValue, [true, false]);
    }

    /**
     * Creates a generic Value object which holds a numeric value
     *
     * @param  {string}            name         The name of the value which is held in storage from session to session
     * @param  {boolean?}          defaultValue The default value to use if none is set. Default: 0
     *
     * @return {Value<number>}
     */
    public static number(name: string, defaultValue = 0) {
      return this.forImpl(new Value(ValueLock, name, defaultValue), (v: number) => typeof v === 'number');
    }

    /**
     * Creates a generic Value object which holds an integer value
     *
     * @param  {string}            name         The name of the value which is held in storage from session to session
     * @param  {boolean?}          defaultValue The default value to use if none is set. Default: 0
     *
     * @return {Value<number>}
     */
    public static int(name: string, defaultValue = 0) {
      return this.forImpl(new Value(ValueLock, name, defaultValue), (v: number) => Number.isInteger(v));
    }

    /**
     * Performs common setup of Value objects
     *
     * @param  {Value<K>}             value     The value which is being initialized
     * @param  {((v: K) => boolean)?} validator The validation function to use when setting new values
     *
     * @return {Value<K>}
     */
    protected static async forImpl<K, V extends Value<K>>(value: V, validator?: ((v: K) => boolean)) {
      if (validator) {
        value.setValidator(validator);
      }

      await value.getValue();

      return value;
    }

    /** The cached value of the latest GM.getValue call */
    #cachedValue: T | undefined;
    /** The default value to use if none is in storage */
    #defaultValue: T;
    /** A function to call when the value is changed, either through initialization or by calling setValue */
    // TODO: Expand to array/set of listeners which can be added or removed dynamically?
    #changeListener: ((oldValue: T, newValue: T) => void) = noop;
    /** The validation function to ensure new values are the correct type/format/etc. before calling GM.setValue */
    #validator: (value: T) => boolean = () => true;

    /**
     * @constructor
     *
     * @param  {symbol} lock         Lock value to prevent direct instantiation
     * @param  {string} name         The name of this Value, the key used when retrieving/storing via GM.getValue/GM.setValue
     * @param  {T}      defaultValue The default value to use if there is none in storage
     */
    protected constructor(lock: symbol, public readonly name: string, defaultValue: T) {
      if (lock !== ValueLock) {
        throw new Error('Please use one of: Value.for, Value.withAllowedValues, Value.boolean, etc. instead.');
      }
      this.#defaultValue = defaultValue;
    }

    /** The current value held in this object, whether it is from storage or the default value */
    public get value() {
      return this.#cachedValue ?? this.#defaultValue;
    }

    /**
     * Sets the validation function for this object
     *
     * @param  {(value: T) => boolean} validator The validation function to use before calling GM.setValue
     *
     * @return {this}
     */
    public setValidator(validator: ((value: T) => boolean)) {
      this.#validator = validator;

      return this;
    }

    /**
     * Get the stored value, if any, for this object, if it has not been retrieved previously
     *
     * @param  {T?} defaultValue The default value to use if none has been stored. Defaults to the value given in the constructor but can be overridden here.
     *
     * @return {Promise<T>}
     */
    public async getValue(defaultValue: T = this.#defaultValue) {
      if (!this.#cachedValue) {
        // NOTE: The value is stringified via JSON.stringify before being stored, so parse the stringified JSON upon retrieval
        const value = JSON.parse(await getValue(this.name) ?? null) ?? defaultValue;

        this.#changeListener(this.value, value);

        this.#cachedValue = value;
      }

      return this.#cachedValue;
    }

    /**
     * Set the stored value for this object
     *
     * @param  {T} value The new value to store
     *
     * @return {this}
     */
    public async setValue(value: T) {
      if (this.#validator(value)) {
        // NOTE: Stringify the value before storing it so that it can be reliably parsed when retrieving
        await setValue(this.name, JSON.stringify(value));

        this.#changeListener(this.value, value);

        this.#cachedValue = value;
      } else {
        console.error(`Failed validation trying to set "${this.name}":`, value);
      }

      return this;
    }

    /**
     * Sets the #changeListener which is called when this Value's cached value is updated
     *
     * @param  {(oldValue: T, newValue: T) => void} listener A function which is called with the previous and new values of this object
     *
     * @return {this}
     */
    public onChange(listener: ((oldValue: T, newValue: T) => void)) {
      this.#changeListener = listener;

      return this;
    }
  }

  /**
   * A Value which has predetermined choices which can be stored in it
   */
  class MultichoiceValue<T = unknown> extends Value<T> {
    /** @overrides Value.withAllowedValues(name: string, defaultValue: K, values: K[]) */
    public static withAllowedValues<K>(name: string, defaultValue: K, values: K[], labels?: string[]) {
      return this.forImpl(new MultichoiceValue(ValueLock, name, defaultValue, values, labels), (v: K) => values.includes(v));
    }

    /**
     * @constructor
     *
     * @param  {symbol} lock         Lock value to prevent direct instantiation
     * @param  {string} name         The name of this Value, the key used when retrieving/storing via GM.getValue/GM.setValue
     * @param  {T}      defaultValue The default value to use if there is none in storage
     * @param  {T[]}    choices      The valid values which can be held in this Value
     */
    protected constructor(
      lock: symbol,
      name: string,
      defaultValue: T,
      public readonly choices: T[],
      public readonly labels?: string[]
    ) {
      super(lock, name, defaultValue);
    }
  }

  /** A Value which holds the column index(es) at which to draw rulers in the editor */
  const RULERS = await Value.for('RULERS', [80, 100, 120], value => (
    Array.isArray(value) &&
    value.every(entry => typeof entry === 'number')
  ));
  /** A Value which holds a boolean determining whether or not trailing whitespace is trimmed from the ends of lines in the file when saving via Ctrl+S */
  const trimTrailingWhitespaceOnSave = await Value.boolean('trimTrailingWhitespaceOnSave');
  /** A Value which holds a boolean determining whether or not a newline is forced at the end of the file when saving via Ctrl+S */
  const ensureNewlineAtEofOnSave = await Value.boolean('ensureNewlineAtEofOnSave');
  /** A Value which holds the name of the theme to use for syntax highlighting in the editor */
  const theme = (await MultichoiceValue.withAllowedValues('theme', 'monokai', ['monokai', 'nord']))
    .onChange((oldValue, newValue) => {
      document.body.classList.remove(`theme-${oldValue}`);
      document.body.classList.add(`theme-${newValue}`);
    });
  /** A Value which holds whether to indent using spaces or tabs */
  const indentStyle = await MultichoiceValue.withAllowedValues('indentStyle', ' ', [' ', '\t'], ['Space', 'Tab']);
  /** A Value which holds the size of indentations (in spaces) or the display width of tab indentation */
  const indentSize = (await Value.int('indentSize', 2))
    .onChange((_oldValue, newValue) => {
      document.body.style.tabSize = newValue.toString();
    });

  /**
   * A debounced function call, which will by default only call itself with the
   * last used arguments after a set delay. New calls to the debounced function
   * will further delay when the underlying implementation is actually called.
   */
  // NOTE: I'm not sure this implementation is 100% correct based but it seems to work fine.
  // Feedback is appreciated.
  class Debounced<T = unknown> {
    /** The wrapped function implementation which is called after the delay */
    readonly #cb: ((...a: T[]) => void);
    /** The number of milliseconds to delay calling the implementation */
    readonly #delay: number;

    /** The time (in milliseconds since epoch) the debounced function was last called */
    #lastRun = 0;
    /** The timeout interval which is updated each time the debounced function is "called", which invokes the implementation after the delay expires */
    #timeout: number | undefined = undefined;

    /**
     * @constructor
     *
     * @param  {(...a: T[]) => void} cb    The wrapped function implementation
     * @param  {number?}             delay The minimum amount of time to delay between allowing calls to pass through to the implementation
     */
    public constructor(cb: ((...a: T[]) => void), delay = 250) {
      this.#cb = cb;
      this.#delay = delay;
    }

    /**
     * Clear timeout information from this debounced function
     */
    public clear() {
      if (this.#timeout) {
        clearTimeout(this.#timeout);
        this.#timeout = undefined;
      }
    }

    /**
     * Immediately run the debounced function implementation, overriding the delay and clearing timeout information
     */
    public flush() {
      this.runImmediate();
      this.clear();
    }

    /**
     * Schedule a run of the debounced implementation with the given arguments
     *
     * @param  {...T[]} args The arguments to use with the debounced function implementation call
     */
    public run(...args: T[]) {
      this.#runImpl(args);
    }

    /**
     * Immediately run the debounced function implementation, overriding the delay
     */
    public runImmediate(...args: T[]) {
      this.#runImpl(args, true);
    }

    /**
     * Schedule a run of the debounced function implementation or run it immediately, optionally overriding the delay
     *
     * @param  {...T[]}  args      The arguments to use with the debounced function implementation call
     * @param  {boolean} immediate Whether or not to override the delay
     */
    #runImpl(args: T[], immediate = false) {
      const now = Date.now();
      const last = now - this.#lastRun;

      this.clear();
      if (immediate || last >= this.#delay) {
        this.#lastRun = now;
        this.#cb(...args);
      } else {
        this.#timeout = setTimeout(() => { this.#cb(...args); }, this.#delay);
      }
    }
  }

  /**
   * Provides undo/redo functionality for the editor, since normal history is
   * lost when modifying contents via JS
   */
  class History<T> extends Array<T> {
    /**
     * Debounced history add function
     *
     * @param  {T} item The history item to add
     */
    readonly #debouncedAdd = new Debounced((item: T) => {
      this.splice(++this.#position);

      if (this.length + 1 > this.#maxSize) {
        this.shift();
      }

      super.push(item);
    });
    /** Maximum size of history */
    readonly #maxSize: number;

    /** Current history position index */
    #position = -1;

    /**
     * @constructor
     *
     * @param  {number?} maxSize The maximum number of history states to hold at once
     */
    public constructor(maxSize: number = 5000) {
      super();

      this.#maxSize = maxSize;
    }

    /**
     * Replace the current history item. Used primarily when updating cursor/selection position
     *
     * @param  {T} item The new history item to place in the history
     */
    public replace(item: T) {
      this[this.#position] = item;
    }

    /**
     * Replace current state with the previous history state
     *
     * @return {T}
     */
    public undo() {
      if (this.#position < 0) {
        throw new Error('No more states to undo');
      }

      return this[--this.#position];
    }

    /**
     * Redo a subseuent history state
     *
     * @return {T}
     */
    public redo() {
      if (this.#position >= this.length) {
        throw new Error('No more states to redo');
      }

      return this[++this.#position];
    }

    /**
     * Add a history state to the stack
     *
     * @param  {T} item The new history item to add to the history stack
     */
    public add(item: T) {
      this.#debouncedAdd.run(item);
    }
  }

  /**
   * Utility function to create a ruler at the specified column number
   *
   * @param  {number} column The column number to render the ruler at
   *
   * @return {HTMLSpanElement}
   */
  function createRuler(column: number) {
    const el = element<HTMLSpanElement>`span.ruler`;
    el.style.left = column + 'ch';

    return el;
  }

  /**
   * Container class for all elements related to syntax highlighting
   */
  class SyntaxHighlight {
    /** Keep track of the index of the current textarea in case there are multiple responses defined */
    public static idx = 0;

    /** Syntax highlighting input history to replace browser textarea history, since certain inputs cause that to be cleared entirely */
    public readonly history = new History<Pick<HTMLTextAreaElement, 'value' | 'selectionStart' | 'selectionEnd' | 'selectionDirection'>>();

    /** Editor gutter for line numbers */
    readonly #gutter = element`.gutter`;
    /** Editor contents */
    readonly #content = element`.content`;
    /** Container for editor rulers */
    readonly #rulers = element<HTMLSpanElement>`span.rulers`;

    /**
     * @constructor
     *
     * @param  {HTMLPreElement} pre The <pre> element containing all elements related to this syntax highlight
     */
    public constructor(public readonly pre: HTMLPreElement) {
      pre.appendChild(this.#gutter);
      pre.appendChild(this.#content);

      RULERS.value.forEach(column => { this.#rulers.appendChild(createRuler(column)); });
      pre.appendChild(this.#rulers);

      RULERS.onChange((_oldValue, newValue) => {
        while (this.#rulers.childNodes.length) {
          this.#rulers.removeChild(this.#rulers.childNodes[0]);
        }
        newValue.forEach(column => { this.#rulers.appendChild(createRuler(column)); });
      });
    }

    /**
     * Set up the correct number of line numbers in the gutter for the current textarea's content
     *
     * @param  {number} numberOfLines
     */
    public setGutterLineNumbers(numberOfLines: number) {
      const length = this.#gutter.childNodes.length;
      const diff = numberOfLines - length;

      if (diff > 0) {
        for (let i = length; i < length + diff; i++) {
          this.#gutter.appendChild(element<HTMLSpanElement>(`span.lineNo ${i + 1}`));
        }
      } else if (diff < 0) {
        for (let i = length - 1; i >= length + diff; i--) {
          this.#gutter.removeChild(this.#gutter.childNodes[i]);
        }
      }
    }

    /** Proxy scrollTop from the containing <pre> element */
    public get scrollTop() {
      return this.pre.scrollTop;
    }
    public set scrollTop(scrollTop: number) {
      this.pre.scrollTop = scrollTop;
    }

    /** Proxy scrollLeft from the content element */
    public get scrollLeft() {
      return this.#content.scrollLeft;
    }
    public set scrollLeft(scrollLeft: number) {
      this.#content.scrollLeft = scrollLeft;
    }

    /** Proxy content innerHTML */
    public get innerHTML() {
      return this.#content.innerHTML;
    }
    public set innerHTML(innerHTML: string) {
      this.#content.innerHTML = innerHTML;
    }
  }

  /**
   * Base menu item, these are intended to be added to the cog menu which is
   * added to the page
   */
  abstract class MenuItem<T = unknown, V extends Value<T> = Value<T>> {
    /** Menu item containing element */
    public readonly element = element<HTMLLIElement>`li.menu-item`;

    /**
     * @constructor
     *
     * @param  {string}   label The menu item's label text
     * @param  {Value<T>} value The value which this menu item uses in order to render its contents
     */
    public constructor(public readonly label: string, public readonly value: V) {
      this.render();
    }

    /** Generic input change handler for this menu item */
    protected abstract onInputChanged(e: Event): void;
    /** Render this menu item's content */
    protected abstract render(): void;

    /**
     * Create a label element for this menu item which is related to the rendered content by element ID
     *
     * @return {HTMLLabelElement}
     */
    protected createLabel() {
      return element<HTMLLabelElement>(`label ${this.label}`, { for: this.value.name });
    }
  }

  /** Boolean (checkbox) menu item */
  class BooleanMenuItem extends MenuItem<boolean> {
    /** @overrides MenuItem#onInputChanged(e: Event) */
    protected onInputChanged = (e: Event) => {
      this.value.setValue((e.target as HTMLInputElement).checked);
    };

    /** @overrides MenuItem#render() */
    protected render() {
      const checkbox = element<HTMLInputElement>(`input#${this.value.name}`, {
        type: 'checkbox',
        checked: this.value.value,
      });

      checkbox.onchange = e => { this.onInputChanged(e); };

      this.element.appendChild(checkbox);
      this.element.appendChild(this.createLabel());
    }
  }

  /** JSON (semi-freeform text) menu item */
  class JsonTextMenuItem<T> extends MenuItem<T> {
    /** @overrides MenuItem#onInputChanged(e: Event) */
    protected onInputChanged = (e: Event) => {
      this.value.setValue(JSON.parse((e.target as HTMLInputElement).value));
    };

     /** @overrides MenuItem#render() */
    protected render() {
      this.element.appendChild(this.createLabel());

      const input = element<HTMLInputElement>(`input#${this.value.name}`, { type: 'text' });
      input.value = JSON.stringify(this.value.value);

      input.onchange = e => { this.onInputChanged(e); };

      this.element.appendChild(input);
    }
  }

  /** Multiple choice menu item */
  // TODO: Figure out a way to genericize this so it will also work with numeric MultichoiceValue objects
  class SelectMenuItem extends MenuItem<string, MultichoiceValue<string>> {
    /** @overrides MenuItem#onInputChanged(e: Event) */
    protected onInputChanged = (e: Event) => {
      this.value.setValue((e.target as HTMLSelectElement).value);
    };

    /** @overrides MenuItem#render() */
    protected render() {
      this.element.appendChild(this.createLabel());

      const select = element<HTMLSelectElement>(`select#${this.value.name}`);

      this.value.choices.forEach((choice, idx) => {
        select.appendChild(element<HTMLOptionElement>(`option ${this.value.labels?.[idx] ?? choice}`, { value: choice }));
      });

      // NOTE: Currently selected value has to be set after adding options to the select
      select.value = this.value.value;
      select.onchange = e => { this.onInputChanged(e); };

      this.element.appendChild(select);
    }
  }

  /** Multiple choice menu item */
  // TODO: Figure out a way to genericize this so it will also work with numeric MultichoiceValue objects
  class RadioMenuItem extends MenuItem<string, MultichoiceValue<string>> {
    /** @overrides MenuItem#onInputChanged(e: Event) */
    protected onInputChanged = (e: Event) => {
      this.value.setValue((e.target as HTMLSelectElement).value);
    };

    /** @overrides MenuItem#render() */
    protected render() {
      this.element.appendChild(this.createLabel());

      const container = element`span.radio-container`;

      this.value.choices.forEach((choice, idx) => {
        const labelText = this.value.labels?.[idx] ?? choice;
        const id = `${this.value.name}-${labelText}`;
        const radio = element<HTMLInputElement>(`input#${id}`, {
          name: this.value.name,
          type: 'radio',
          checked: choice === this.value.value
        });

        radio.value = choice;
        radio.onchange = e => { this.onInputChanged(e); };

        container.appendChild(radio);

        container.appendChild(element<HTMLLabelElement>(`label ${labelText}`, { for: id }));
      });

      this.element.appendChild(container);
    }
  }

  /**
   * Menu container for menu items, with hide/show capabilities
   */
  class Menu {
    /** Base menu container element */
    public readonly element = element<HTMLSpanElement>`span.menu`;

    /**
     * @constructor
     *
     * @param  {MenuItem<any>[]} items Menu items to add to this menu
     */
    // deno-lint-ignore no-explicit-any
    public constructor(items: MenuItem<any>[]) {
      const header = element<HTMLLabelElement>(`label.editor-options-menu-header Editor Options`, {
        for: 'editor-options-menu',
      });

      header.appendChild(element<HTMLSpanElement>`i.fas.fa-cog`);

      this.element.appendChild(header);

      const menu = element<HTMLMenuElement>(`menu#editor-options-menu`, { hidden: true });

      // Show/hide the menu items on alternating clicks of the cog icon
      header.onclick = e => {
        if (e.button !== 0) {
          return;
        }
        if (menu.hasAttribute('hidden')) {
          menu.removeAttribute('hidden');
        } else {
          menu.setAttribute('hidden', 'true');
        }
        if (header.classList.contains('open')) {
          header.classList.remove('open');
        } else {
          header.classList.add('open');
        }
      };

      for (const item of items) {
        menu.appendChild(item.element);
      }

      this.element.appendChild(menu);
    }
  }

  /** Track SyntaxHighlight objects by the textarea they are tied to */
  const inputs = new Map<HTMLTextAreaElement, SyntaxHighlight>();
  
  /**
   * Create the HTML markup for a <span> element with the given class and text content
   *
   * @param  {string} clazz   The classname to apply to the span
   * @param  {string} content The text content of the span
   *
   * @return {string}
   */
  function span(clazz: string, content: string) {
    return `<span class="${clazz}">${content.replace(/</g, '&lt;').replace(/>/g, '&gt;')}</span>`;
  }
  
  /**
   * Highlight Discord pings in a given segment of text
   *
   * @param  {string} segment The text to highlight pings inside of
   *
   * @return {string}
   */
  function highlightPings(segment: string) {
    switch (segment) {
      case '>': /* falls through */
      case '<@': /* falls through */
      case '<@&': /* falls through */
      case '<#': return span('mention', segment);
      default:
        if (/<(?:[@#]&?\d+|(?::[^:]+:)?\d+)>/.test(segment)) {
          return span('mention', segment);
        }

        return segment;
    }
  }

  /**
   * Classify the given segment of text
   *
   * @param  {string} segment
   *
   * @return {string}
   */
  function classify(segment: string) {
    if (!segment || !segment.trim()) {
      return segment;
    }
    
    switch(segment) {
      case '(': /* falls through */
      case ')': return span('paren', segment);
      case '{{': /* falls through */
      case '}}': return span('braces', segment);
      case 'if': /* falls through */
      case 'with': /* falls through */
      case 'else': /* falls through */
      case 'range': /* falls through */
      case 'end': /* falls through */
      case 'return': /* falls through */
      case 'break': /* falls through */
      case 'try': /* falls through */
      case 'catch': /* falls through */
      case 'continue': return span('control', segment);
      case 'define': /* falls through */
      case 'block': return span('keyword', segment);
      case 'and': /* falls through */
      case 'or': /* falls through */
      case 'not': return span('rel', segment);
      case 'eq': /* falls through */
      case 'ne': /* falls through */
      case 'lt': /* falls through */
      case 'le': /* falls through */
      case 'gt': /* falls through */
      case 'ge': return span('comp', segment);
      case ':=': /* falls through */
      case '=': return span('assign', segment);
      case '&': /* falls through */
      case '|': /* falls through */
      case '/': /* falls through */
      case '+': /* falls through */
      case '*': /* falls through */
      case '-': /* falls through */
      case '%': return span('op', segment);
      case 'nil': return span('lit', segment);
      case 'true': /* falls through */
      case 'false': return span('bool', segment);
      case ',': /* falls through */
      case '.': return span('symbol', segment);
      default:
        if (segment.startsWith('$')) {
          return span('var', segment)
        }
        if (segment.startsWith("'")) {
          return span(
            segment.endsWith("'") && (segment.length === 3 || (segment.length === 4 && segment.charAt(1) === '\\'))
              ? 'string'
              : 'error',
            segment,
          )
            .replace(/\\./g, lit => span('lit', lit));
        }
        if (segment.startsWith('"')) {
          return span((!segment.endsWith('"') || /(?<!\\)\\"$/.test(segment)) ? 'error' : 'string', segment)
            .replace(/\\./g, lit => span('lit', lit))
            .replace(/\%(?:\d*?\.?\d*?)?[dsfxt]/g, fmtr => span('formatter', fmtr))
            .replace(/&lt;(?:[@#]&?\d+|(?::[^:]+:)?\d+)&gt;/g, mention => span('mention', mention));
        }
        if (segment.endsWith('"') && !segment.startsWith('"')) {
          return span('error', segment);
        }
        if (segment.startsWith('/*')) {
          return span(!segment.endsWith('*/') ? 'error' : 'comment', segment);
        }
        if (segment.endsWith('*/') && !segment.startsWith('/*')) {
          return span('error', segment);
        }
        if (/^\d+$/.test(segment)) {
          return span('number', segment);
        }

        return span('builtin', segment);
    }
  }

  /**
   * Build syntax highlighting for the current textarea content
   *
   * @param  {InputEvent} e A keystroke, typically
   */
  function syntaxHighlight(e: InputEvent) {
    const el = e.target as HTMLTextAreaElement;
    const overlay = inputs.get(el);
    if (!overlay) {
      return;
    }

    const { value, selectionStart, selectionEnd, selectionDirection } = el;

    if (!e.inputType.startsWith('history')) {
      overlay.history.add({ value, selectionStart, selectionEnd, selectionDirection });
    }

    // TODO: Come up with a better (i.e.: more robust) system of parsing the code and inserting it into the overlay
    const highlighted = value
      .replace(/(?<=\}{2})[\s\S]*?(?=\{{2})/g, segment => segment
        .split(/(\s+|[.'"`:])/g)
        .map(highlightPings)
        .join('')
      )
      .replace(/\{{2}[\s\S]*?\}{2}/g, segment => segment
        .split(/([{}]{2}|".*?(?<!\\)"|'.*?'|\/\*[\s\S]*?(?:\*\/)|:?=|[().,]|\s+)/g)
        .map(classify)
        .join('')
      );

    overlay.innerHTML = highlighted;
    overlay.setGutterLineNumbers(highlighted.split('\n').length);
  }

  /**
   * Match scroll position between a textarea and its SyntaxHighlight overlay
   *
   * @param  {Event} e Keystroke, mousemove, or scroll event which changed textarea scroll position
   */
  function matchScrollPos(e: Event) {
    const el = e.target as HTMLTextAreaElement;
    const overlay = inputs.get(el);
    if (!overlay) {
      return;
    }

    overlay.scrollTop = el.scrollTop;
    overlay.scrollLeft = el.scrollLeft;
  }

  /** Characters which, when typed, automatically add their matched character immediately afterward */
  const autoMatch: Record<string, string> = {
    '{': '}',
    '(': ')',
    '[': ']',
    '"': '"',
    "'": "'",
  };
  /** Characters which, if they already exist as the next character in the text, are not added when the key is pressed */
  const skipIfExisting = Object.values(autoMatch);

  /**
   * Override default typing behavior for select keystrokes and apply syntax highlighting anew for the editor
   *
   * @param  {KeyboardEvent} e A keydown event
   */
  function handleKeydown(e: KeyboardEvent) {
    const el = e.target as HTMLTextAreaElement;
    const overlay = inputs.get(el);
    if (!overlay) {
      return;
    }
    const { value, selectionStart, selectionEnd, selectionDirection } = el;

    const selectionSize = selectionEnd - selectionStart;
    const beforeSelection = value.substring(0, selectionStart);
    const selection = value.substring(selectionStart, selectionEnd);
    const afterSelection = value.substring(selectionEnd);

    let inserted = '';
    let result;
    let newStart;
    let newEnd;

    const key = e.key.toLowerCase();

    // NOTE: Yes, I use Windows. If I get enough people asking for it I'll add logic to use Cmd on Mac (and optionally Alt on Windows).
    // TODO: This is very unclean, try to figure out a better way of doing this
    if (key === '/' && e.ctrlKey) {
      const blockStart = beforeSelection.lastIndexOf('{{') + 2;
      let blockEnd = value.indexOf('}}', selectionEnd);
      let block = value.substring(blockStart, blockEnd);
      if (block.includes('}}') || block.includes('{{')) {
        blockEnd = value.indexOf('}}', blockStart);
        block = value.substring(blockStart, blockEnd);
      }
      if (block.startsWith('/*') && block.endsWith('*/')) {
        block = block.replace(/^\/\*\s*|\s*\*\/$/g, '');
        newStart = blockStart;
        newEnd = blockStart + block.length;
      } else {
        block = `/* ${block} */`;
        newStart = blockStart + 3;
        newEnd = blockStart + 3;
      }
      inserted = block;
      result = value.substring(0, blockStart) + inserted + value.substring(blockEnd);
    } else if (key === 'd' && e.ctrlKey && e.shiftKey) {
      if (selectionSize > 0) {
        inserted = selection;
        result = beforeSelection + selection + selection + afterSelection;
        newStart = selectionEnd;
        newEnd = selectionEnd + selectionSize;
      } else {
        const blockStart = beforeSelection.lastIndexOf('\n');
        const blockEnd = value.indexOf('\n', selectionEnd);
        const block = value.substring(blockStart, blockEnd);
        inserted = block;
        result = value.substring(0, blockEnd) + inserted + value.substring(blockEnd);
        newStart = selectionStart + block.length;
        newEnd = selectionEnd + block.length;
      }
    } else if (skipIfExisting.includes(key) && selectionSize === 0 && value.substring(selectionEnd, selectionEnd + 1) === key) {
      e.preventDefault();
      e.stopPropagation();
      el.selectionStart = selectionStart + 1;
      el.selectionEnd = selectionEnd + 1;
      return;
    } else if (key in autoMatch) {
      inserted = key + selection + autoMatch[key];
      result = beforeSelection + inserted + afterSelection;
      newStart = selectionStart + 1;
      newEnd = selectionEnd + 1;
    } else if (key === 'backspace' && selectionSize === 0) {
      const prev = value.substring(selectionStart - 1, selectionStart);
      const next = value.substring(selectionEnd, selectionEnd + 1);

      if ((prev === '{' && next === '}') || (prev === '(' && next === ')') || (prev === '"' && next === '"')) {
        result = value.substring(0, selectionStart - 1) + value.substring(selectionEnd + 1);
        newStart = selectionStart - 1;
        newEnd = selectionEnd - 1;
      }
    } else if (key === 'tab' && !e.ctrlKey) {
      const atBeginningOfLine = value.charAt(selectionStart - 1) === '\n';

      const _indentStyle = indentStyle.value;
      const indentation = _indentStyle === ' '
        ? _indentStyle.repeat(indentSize.value)
        : _indentStyle;
      const indentLength = indentation.length;

      if (e.shiftKey && (value.substring(selectionStart - indentLength, selectionStart) === indentation || atBeginningOfLine)) {
        if (atBeginningOfLine) {
          if (value.substring(selectionStart, selectionStart + indentLength) === '  ') {
            result = beforeSelection + value.substring(selectionStart + indentLength);
          }
        } else {
          result = value.substring(0, selectionStart - indentLength) + selection + afterSelection;
          newStart = selectionStart - indentLength;
          newEnd = newStart;
        }
      } else {
        inserted = indentation;
        result = beforeSelection + inserted + selection + afterSelection;
        newStart = selectionStart + indentLength;
        newEnd = newStart;
      }
    } else if (key === 'enter') {
      const currentLineStart = beforeSelection.lastIndexOf('\n') + 1;
      const currentLineIndentation = value.substring(currentLineStart, selectionStart).match(/^\s*/)?.[0] ?? '';
      inserted = '\n' + currentLineIndentation;
      result = beforeSelection + inserted + afterSelection;
      newStart = selectionEnd + inserted.length;
      newEnd = selectionEnd + inserted.length;
    }

    if (['arrowup', 'arrowleft', 'arrowright', 'arrowdown'].includes(key)) {
      overlay.history.replace({
        value: result ?? value,
        selectionStart: newStart ?? selectionStart,
        selectionEnd: newEnd ?? selectionEnd,
        selectionDirection,
      });
    }

    let evt: InputEvent | undefined;

    if (e.ctrlKey && (key === 'z' || key === 'y')) {
      try {
        const undo = key === 'z' && !e.shiftKey
        const state = undo
          ? overlay.history.undo()
          : overlay.history.redo();

        el.value = state.value;
        el.selectionStart = state.selectionStart;
        el.selectionEnd = state.selectionEnd;
        el.selectionDirection = state.selectionDirection;

        evt = new InputEvent('input', {
          data: '',
          inputType: undo ? 'historyUndo' : 'historyRedo',
        });
      } catch {
        /* Don't care - likely no state to perform */
      }
    } else if (result) {
      el.value = result;
      el.selectionStart = newStart ?? selectionStart;
      el.selectionEnd = newEnd ?? selectionEnd;

      evt = new InputEvent('input', {
        data: inserted,
        inputType: inserted === '' ? 'deleteContent' : 'insertText',
      });
    }

    if (evt) {
      e.preventDefault();
      e.stopPropagation();

      el.dispatchEvent(evt);
    }
  }

  /**
   * Update the current selection position in history by replacing the current history entry when clicking
   *
   * @param  {MouseEvent} e The mouse event that may have changed the cursor position
   */
  function updateSelectionPosition(e: MouseEvent) {
    const el = e.target as HTMLTextAreaElement;
    const overlay = inputs.get(el);
    if (!overlay) {
      return;
    }
    // Update current selection position but don't push a new state onto the stack
    overlay.history.replace({
      value: el.value,
      selectionStart: el.selectionStart,
      selectionEnd: el.selectionEnd,
      selectionDirection: el.selectionDirection,
    });
  }

  // Add the appropriate class for the current theme to the document body, so that all textareas have the correct syntax highlighting
  document.body.classList.add(`theme-${theme.value}`);
  // Set the display width for tab indentation
  document.body.style.tabSize = indentSize.value.toString();

  // When the page first renders, scan for all appropriate textareas and add syntax highlighting + associated event listeners to them
  const interval = setInterval(() => {
    const textAreas = Array.from(document.querySelectorAll('textarea[placeholder="Command body here"]') as NodeListOf<HTMLTextAreaElement>);

    const unmappedTextAreas = textAreas.filter(ta => !inputs.has(ta));

    if (textAreas.length > 0 && unmappedTextAreas.length === 0) {
      // All textareas have been mapped, stop the interval
      clearInterval(interval);
    }

    for (const textArea of unmappedTextAreas) {
      textArea.parentElement?.querySelectorAll('.syntax-highlight').forEach(overlay => {
        overlay.parentElement?.removeChild(overlay);
      });

      textArea.spellcheck = false;

      const overlay = new SyntaxHighlight(element<HTMLPreElement>(`pre#syntax-highlighting-${SyntaxHighlight.idx++}.syntax-highlight`));
      textArea.parentElement?.appendChild(overlay.pre);

      textArea.addEventListener('input', syntaxHighlight as (e: Event) => void);
      textArea.addEventListener('scroll', matchScrollPos);
      textArea.addEventListener('keydown', handleKeydown);
      textArea.addEventListener('mouseup', updateSelectionPosition);

      inputs.set(textArea, overlay);

      syntaxHighlight({ target: textArea, inputType: 'insertText' } as unknown as InputEvent);
      matchScrollPos({ target: textArea } as unknown as Event);
    }
  }, 100);

  // Add a document-wide keydown event listener for Ctrl+S which performs cleanup actions on all known textareas and automatically clicks the "Save" button
  self.addEventListener('keydown', (e: KeyboardEvent) => {
    if (e.key == 's' && e.ctrlKey) {
      e.preventDefault();
      e.stopPropagation();

      for (const [textarea] of inputs) {
        if (trimTrailingWhitespaceOnSave.value) {
          // TODO: Figure out how to move the cursor if it's inside the whitespace being trimmed here
          textarea.value = textarea.value.replace(/(?<=\w)\s*(?=\n)/g, '');
        }
        if (ensureNewlineAtEofOnSave.value) {
          textarea.value = textarea.value.trimEnd() + '\n';
        }
        syntaxHighlight({ target: textarea, inputType: 'insertText' } as unknown as InputEvent);
      }

      (document.querySelector('form[action$="update"] button.btn[type="submit"]') as HTMLButtonElement)?.click();
    }
  });

  // Add the GUI menu allowing the user to edit runtime behavior variables
  (document.querySelector('#cc-feedback-top + form > div.row.mb-2') ?? document.body).appendChild(
    new Menu([
      new SelectMenuItem('Theme', theme),
      new RadioMenuItem('Indent Style', indentStyle),
      new JsonTextMenuItem('Indent Size', indentSize),
      new JsonTextMenuItem('Rulers', RULERS),
      new BooleanMenuItem('Trim trailing whitespace on save', trimTrailingWhitespaceOnSave),
      new BooleanMenuItem('Ensure newline at EOF on save', ensureNewlineAtEofOnSave),
    ]).element
  );
})();

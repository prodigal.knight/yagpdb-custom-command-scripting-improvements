// ==UserScript==
// @name        YAGPDB Custom Commands Syntax Highlighting
// @description Adds syntax highlighting to YAGPDB Custom Command editor (as well as a few other handy features)
// @version     1.4.4
// @grant       GM.getValue
// @grant       GM.setValue
// @match       https://yagpdb.xyz/manage/*/customcommands/commands/*
// ==/UserScript==
(async ()=>{
    let { getValue , setValue  } = GM;
    function o(a) {
        if (null == a) throw new Error(`Expected a value, got "${a}"`);
        return a;
    }
    function h(g = '', j = {}) {
        let k = g.raw?.join('') ?? g, c = function*(b) {
            let e = b.length, c = 0, a = 0;
            out: for(; a < e;){
                let d = b.charAt(a);
                switch(d){
                    case '.':
                    case '#':
                    case ' ':
                        if (yield b.slice(c, a), yield d, c = ++a, ' ' === d) break out;
                    default:
                        a++;
                }
            }
            yield b.slice(c);
        }(k), a, h = '', d = '', e = [], f = '';
        for(; !(a = c.next()).done;)'#' === a.value ? d = o((a = c.next()).value) : '.' === a.value ? e.push(o((a = c.next()).value)) : ' ' === a.value ? f = o((a = c.next()).value) : h = o(a.value);
        let b = document.createElement(h || 'div');
        for (let [l, i] of (d && b.setAttribute('id', d), e.length && b.classList.add(...e), Object.entries(j)))!1 !== i && b.setAttribute(l, i.toString());
        return f && b.appendChild(document.createTextNode(f)), b;
    }
    let p = Symbol('ValueLock');
    class a {
        static for(b, c, d) {
            return this.a(new a(p, b, c), d);
        }
        static b(b, c, d) {
            return this.a(new a(p, b, c), (a)=>d.includes(a));
        }
        static c(a, b = !0) {
            return this.b(a, b, [
                !0,
                !1
            ]);
        }
        static number(b, c = 0) {
            return this.a(new a(p, b, c), (a)=>'number' == typeof a);
        }
        static d(b, c = 0) {
            return this.a(new a(p, b, c), (a)=>Number.isInteger(a));
        }
        static async a(a, b) {
            return b && a.e(b), await a.f(), a;
        }
        #a;
        #b;
        #c;
        #d;
        constructor(a, b, c){
            if (this.name = b, this.#c = ()=>{}, this.#d = ()=>!0, a !== p) throw new Error('Please use one of: Value.for, Value.withAllowedValues, Value.boolean, etc. instead.');
            this.#b = c;
        }
        get value() {
            return this.#a ?? this.#b;
        }
        e(a) {
            return this.#d = a, this;
        }
        async f(b = this.#b) {
            if (!this.#a) {
                let a = JSON.parse(await getValue(this.name) ?? null) ?? b;
                this.#c(this.value, a), this.#a = a;
            }
            return this.#a;
        }
        async g(a) {
            return this.#d(a) ? (await setValue(this.name, JSON.stringify(a)), this.#c(this.value, a), this.#a = a) : console.error(`Failed validation trying to set "${this.name}":`, a), this;
        }
        h(a) {
            return this.#c = a, this;
        }
    }
    class b extends a {
        static b(a, c, d, e) {
            return this.a(new b(p, a, c, d, e), (a)=>d.includes(a));
        }
        constructor(a, b, c, d, e){
            super(a, b, c), this.choices = d, this.labels = e;
        }
    }
    let i = await a.for('RULERS', [
        80,
        100,
        120
    ], (a)=>Array.isArray(a) && a.every((a)=>'number' == typeof a)), j = await a.c('trimTrailingWhitespaceOnSave'), k = await a.c('ensureNewlineAtEofOnSave'), c = (await b.b('theme', 'monokai', [
        'monokai',
        'nord'
    ])).h((a, b)=>{
        document.body.classList.remove(`theme-${a}`), document.body.classList.add(`theme-${b}`);
    }), l = await b.b('indentStyle', ' ', [
        ' ',
        '\t'
    ], [
        'Space',
        'Tab'
    ]), d = (await a.d('indentSize', 2)).h((b, a)=>{
        document.body.style.tabSize = a.toString();
    });
    class q extends Array {
        #e = new class {
            #f;
            #g;
            #h = 0;
            #i = void 0;
            constructor(a, b = 250){
                this.#f = a, this.#g = b;
            }
            clear() {
                this.#i && (clearTimeout(this.#i), this.#i = void 0);
            }
            flush() {
                this.i(), this.clear();
            }
            j(...a) {
                this.#j(a);
            }
            i(...a) {
                this.#j(a, !0);
            }
             #j(b, c = !1) {
                let a = Date.now(), d = a - this.#h;
                this.clear(), c || d >= this.#g ? (this.#h = a, this.#f(...b)) : this.#i = setTimeout(()=>{
                    this.#f(...b);
                }, this.#g);
            }
        }((a)=>{
            this.splice(++this.#k), this.length + 1 > this.#l && this.shift(), super.push(a);
        });
        #l;
        #k = -1;
        constructor(a = 5000){
            super(), this.#l = a;
        }
        replace(a) {
            this[this.#k] = a;
        }
        k() {
            if (this.#k < 0) throw new Error('No more states to undo');
            return this[--this.#k];
        }
        l() {
            if (this.#k >= this.length) throw new Error('No more states to redo');
            return this[++this.#k];
        }
        add(a) {
            this.#e.j(a);
        }
    }
    function r(b) {
        let a = h`span.ruler`;
        return a.style.left = b + 'ch', a;
    }
    class s {
        static m = 0;
        #m;
        #n;
        #o;
        constructor(a){
            this.n = a, this.history = new q(), this.#m = h`.gutter`, this.#n = h`.content`, this.#o = h`span.rulers`, a.appendChild(this.#m), a.appendChild(this.#n), i.value.forEach((a)=>{
                this.#o.appendChild(r(a));
            }), a.appendChild(this.#o), i.h((b, a)=>{
                for(; this.#o.childNodes.length;)this.#o.removeChild(this.#o.childNodes[0]);
                a.forEach((a)=>{
                    this.#o.appendChild(r(a));
                });
            });
        }
        o(e) {
            let a = this.#m.childNodes.length, b = e - a;
            if (b > 0) for(let c = a; c < a + b; c++)this.#m.appendChild(h(`span.lineNo ${c + 1}`));
            else if (b < 0) for(let d = a - 1; d >= a + b; d--)this.#m.removeChild(this.#m.childNodes[d]);
        }
        get scrollTop() {
            return this.n.scrollTop;
        }
        set scrollTop(a) {
            this.n.scrollTop = a;
        }
        get scrollLeft() {
            return this.#n.scrollLeft;
        }
        set scrollLeft(a) {
            this.#n.scrollLeft = a;
        }
        get innerHTML() {
            return this.#n.innerHTML;
        }
        set innerHTML(a) {
            this.#n.innerHTML = a;
        }
    }
    class e {
        constructor(a, b){
            this.label = a, this.value = b, this.element = h`li.menu-item`, this.p();
        }
        q() {
            return h(`label ${this.label}`, {
                for: this.value.name
            });
        }
    }
    class f extends e {
        r = (a)=>{
            this.value.g(a.target.checked);
        };
        p() {
            let a = h(`input#${this.value.name}`, {
                type: 'checkbox',
                checked: this.value.value
            });
            a.onchange = (a)=>{
                this.r(a);
            }, this.element.appendChild(a), this.element.appendChild(this.q());
        }
    }
    class g extends e {
        r = (a)=>{
            this.value.g(JSON.parse(a.target.value));
        };
        p() {
            this.element.appendChild(this.q());
            let a = h(`input#${this.value.name}`, {
                type: 'text'
            });
            a.value = JSON.stringify(this.value.value), a.onchange = (a)=>{
                this.r(a);
            }, this.element.appendChild(a);
        }
    }
    class m {
        element = h`span.menu`;
        constructor(c){
            let a = h("label.editor-options-menu-header Editor Options", {
                for: 'editor-options-menu'
            });
            a.appendChild(h`i.fas.fa-cog`), this.element.appendChild(a);
            let b = h("menu#editor-options-menu", {
                hidden: !0
            });
            for (let d of (a.onclick = (c)=>{
                0 === c.button && (b.hasAttribute('hidden') ? b.removeAttribute('hidden') : b.setAttribute('hidden', 'true'), a.classList.contains('open') ? a.classList.remove('open') : a.classList.add('open'));
            }, c))b.appendChild(d.element);
            this.element.appendChild(b);
        }
    }
    let t = new Map();
    function u(a, b) {
        return `<span class="${a}">${b.replace(/</g, '&lt;').replace(/>/g, '&gt;')}</span>`;
    }
    function v(a) {
        switch(a){
            case '>':
            case '<@':
            case '<@&':
            case '<#':
                return u('mention', a);
            default:
                if (/<(?:[@#]&?\d+|(?::[^:]+:)?\d+)>/.test(a)) return u('mention', a);
                return a;
        }
    }
    function w(a) {
        if (!a || !a.trim()) return a;
        switch(a){
            case '(':
            case ')':
                return u('paren', a);
            case '{{':
            case '}}':
                return u('braces', a);
            case 'if':
            case 'with':
            case 'else':
            case 'range':
            case 'end':
            case 'return':
            case 'break':
            case 'try':
            case 'catch':
            case 'continue':
                return u('control', a);
            case 'define':
            case 'block':
                return u('keyword', a);
            case 'and':
            case 'or':
            case 'not':
                return u('rel', a);
            case 'eq':
            case 'ne':
            case 'lt':
            case 'le':
            case 'gt':
            case 'ge':
                return u('comp', a);
            case ':=':
            case '=':
                return u('assign', a);
            case '&':
            case '|':
            case '/':
            case '+':
            case '*':
            case '-':
            case '%':
                return u('op', a);
            case 'nil':
                return u('lit', a);
            case 'true':
            case 'false':
                return u('bool', a);
            case ',':
            case '.':
                return u('symbol', a);
            default:
                if (a.startsWith('$')) return u('var', a);
                if (a.startsWith("'")) return u(a.endsWith("'") && (3 === a.length || 4 === a.length && '\\' === a.charAt(1)) ? 'string' : 'error', a).replace(/\\./g, (a)=>u('lit', a));
                if (a.startsWith('"')) return u(!a.endsWith('"') || /(?<!\\)\\"$/.test(a) ? 'error' : 'string', a).replace(/\\./g, (a)=>u('lit', a)).replace(/\%(?:\d*?\.?\d*?)?[dsfxt]/g, (a)=>u('formatter', a)).replace(/&lt;(?:[@#]&?\d+|(?::[^:]+:)?\d+)&gt;/g, (a)=>u('mention', a));
                if (a.endsWith('"') && !a.startsWith('"')) return u('error', a);
                if (a.startsWith('/*')) return u(a.endsWith('*/') ? 'comment' : 'error', a);
                if (a.endsWith('*/') && !a.startsWith('/*')) return u('error', a);
                if (/^\d+$/.test(a)) return u('number', a);
                return u('builtin', a);
        }
    }
    function x(b) {
        let c = b.target, a = t.get(c);
        if (!a) return;
        let { value: d , selectionStart: f , selectionEnd: g , selectionDirection: h  } = c;
        b.inputType.startsWith('history') || a.history.add({
            value: d,
            selectionStart: f,
            selectionEnd: g,
            selectionDirection: h
        });
        let e = d.replace(/(?<=\}{2})[\s\S]*?(?=\{{2})/g, (a)=>a.split(/(\s+|[.'"`:])/g).map(v).join('')).replace(/\{{2}[\s\S]*?\}{2}/g, (a)=>a.split(/([{}]{2}|".*?(?<!\\)"|'.*?'|\/\*[\s\S]*?(?:\*\/)|:?=|[().,]|\s+)/g).map(w).join(''));
        a.innerHTML = e, a.o(e.split('\n').length);
    }
    function y(c) {
        let a = c.target, b = t.get(a);
        b && (b.scrollTop = a.scrollTop, b.scrollLeft = a.scrollLeft);
    }
    let n = {
        "s": '}',
        "t": ')',
        "u": ']',
        "v": '"',
        "w": "'"
    }, z = Object.values(n);
    function A(f) {
        let g = f.target, u = t.get(g);
        if (!u) return;
        let { value: a , selectionStart: b , selectionEnd: c , selectionDirection: I  } = g, v = c - b, o = a.substring(0, b), q = a.substring(b, c), s = a.substring(c), h = '', i, j, k, e = f.key.toLowerCase();
        if ('/' === e && f.ctrlKey) {
            let p = o.lastIndexOf('{{') + 2, w = a.indexOf('}}', c), m = a.substring(p, w);
            (m.includes('}}') || m.includes('{{')) && (w = a.indexOf('}}', p), m = a.substring(p, w)), m.startsWith('/*') && m.endsWith('*/') ? (m = m.replace(/^\/\*\s*|\s*\*\/$/g, ''), j = p, k = p + m.length) : (m = `/* ${m} */`, j = p + 3, k = p + 3), h = m, i = a.substring(0, p) + h + a.substring(w);
        } else if ('d' === e && f.ctrlKey && f.shiftKey) {
            if (v > 0) h = q, i = o + q + q + s, j = c, k = c + v;
            else {
                let J = o.lastIndexOf('\n'), A = a.indexOf('\n', c), B = a.substring(J, A);
                h = B, i = a.substring(0, A) + h + a.substring(A), j = b + B.length, k = c + B.length;
            }
        } else if (z.includes(e) && 0 === v && a.substring(c, c + 1) === e) {
            f.preventDefault(), f.stopPropagation(), g.selectionStart = b + 1, g.selectionEnd = c + 1;
            return;
        } else if (e in n) i = o + (h = e + q + n[e]) + s, j = b + 1, k = c + 1;
        else if ('backspace' === e && 0 === v) {
            let C = a.substring(b - 1, b), D = a.substring(c, c + 1);
            ('{' === C && '}' === D || '(' === C && ')' === D || '"' === C && '"' === D) && (i = a.substring(0, b - 1) + a.substring(c + 1), j = b - 1, k = c - 1);
        } else if ('tab' !== e || f.ctrlKey) {
            if ('enter' === e) {
                let K = o.lastIndexOf('\n') + 1, L = a.substring(K, b).match(/^\s*/)?.[0] ?? '';
                i = o + (h = '\n' + L) + s, j = c + h.length, k = c + h.length;
            }
        } else {
            let G = '\n' === a.charAt(b - 1), E = l.value, F = ' ' === E ? E.repeat(d.value) : E, r = F.length;
            f.shiftKey && (a.substring(b - r, b) === F || G) ? G ? '  ' === a.substring(b, b + r) && (i = o + a.substring(b + r)) : (i = a.substring(0, b - r) + q + s, j = b - r, k = j) : (i = o + (h = F) + q + s, k = j = b + r);
        }
        [
            'arrowup',
            'arrowleft',
            'arrowright',
            'arrowdown'
        ].includes(e) && u.history.replace({
            value: i ?? a,
            selectionStart: j ?? b,
            selectionEnd: k ?? c,
            selectionDirection: I
        });
        let x;
        if (f.ctrlKey && ('z' === e || 'y' === e)) try {
            let H = 'z' === e && !f.shiftKey, y = H ? u.history.k() : u.history.l();
            g.value = y.value, g.selectionStart = y.selectionStart, g.selectionEnd = y.selectionEnd, g.selectionDirection = y.selectionDirection, x = new InputEvent('input', {
                data: '',
                inputType: H ? 'historyUndo' : 'historyRedo'
            });
        } catch  {}
        else i && (g.value = i, g.selectionStart = j ?? b, g.selectionEnd = k ?? c, x = new InputEvent('input', {
            data: h,
            inputType: '' === h ? 'deleteContent' : 'insertText'
        }));
        x && (f.preventDefault(), f.stopPropagation(), g.dispatchEvent(x));
    }
    function B(c) {
        let a = c.target, b = t.get(a);
        b && b.history.replace({
            value: a.value,
            selectionStart: a.selectionStart,
            selectionEnd: a.selectionEnd,
            selectionDirection: a.selectionDirection
        });
    }
    document.body.classList.add(`theme-${c.value}`), document.body.style.tabSize = d.value.toString();
    let C = setInterval(()=>{
        let b = Array.from(document.querySelectorAll('textarea[placeholder="Command body here"]')), c = b.filter((a)=>!t.has(a));
        for (let a of (b.length > 0 && 0 === c.length && clearInterval(C), c)){
            a.parentElement?.querySelectorAll('.syntax-highlight').forEach((a)=>{
                a.parentElement?.removeChild(a);
            }), a.spellcheck = !1;
            let d = new s(h(`pre#syntax-highlighting-${s.m++}.syntax-highlight`));
            a.parentElement?.appendChild(d.n), a.addEventListener('input', x), a.addEventListener('scroll', y), a.addEventListener('keydown', A), a.addEventListener('mouseup', B), t.set(a, d), x({
                target: a,
                inputType: 'insertText'
            }), y({
                target: a
            });
        }
    }, 100);
    self.addEventListener('keydown', (b)=>{
        if ('s' == b.key && b.ctrlKey) {
            for (let [a] of (b.preventDefault(), b.stopPropagation(), t))j.value && (a.value = a.value.replace(/(?<=\w)\s*(?=\n)/g, '')), k.value && (a.value = a.value.trimEnd() + '\n'), x({
                target: a,
                inputType: 'insertText'
            });
            document.querySelector('form[action$="update"] button.btn[type="submit"]')?.click();
        }
    }), (document.querySelector('#cc-feedback-top + form > div.row.mb-2') ?? document.body).appendChild(new m([
        new class extends e {
            r = (a)=>{
                this.value.g(a.target.value);
            };
            p() {
                this.element.appendChild(this.q());
                let a = h(`select#${this.value.name}`);
                this.value.choices.forEach((b, c)=>{
                    a.appendChild(h(`option ${this.value.labels?.[c] ?? b}`, {
                        value: b
                    }));
                }), a.value = this.value.value, a.onchange = (a)=>{
                    this.r(a);
                }, this.element.appendChild(a);
            }
        }('Theme', c),
        new class extends e {
            r = (a)=>{
                this.value.g(a.target.value);
            };
            p() {
                this.element.appendChild(this.q());
                let a = h`span.radio-container`;
                this.value.choices.forEach((b, f)=>{
                    let d = this.value.labels?.[f] ?? b, e = `${this.value.name}-${d}`, c = h(`input#${e}`, {
                        name: this.value.name,
                        type: 'radio',
                        checked: b === this.value.value
                    });
                    c.value = b, c.onchange = (a)=>{
                        this.r(a);
                    }, a.appendChild(c), a.appendChild(h(`label ${d}`, {
                        for: e
                    }));
                }), this.element.appendChild(a);
            }
        }('Indent Style', l),
        new g('Indent Size', d),
        new g('Rulers', i),
        new f('Trim trailing whitespace on save', j),
        new f('Ensure newline at EOF on save', k), 
    ]).element);
})();
